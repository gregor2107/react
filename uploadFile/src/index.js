import React from 'react';
import ReactDOM from 'react-dom';
import './../node_modules/font-awesome/css/font-awesome.min.css';
import './../node_modules/react-bootstrap/dist/react-bootstrap.min.js';
import './../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './index.css';

import ZadanieReact from './App';

ReactDOM.render(<ZadanieReact />,
  document.getElementById('root')
);

