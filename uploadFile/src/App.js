import React, {Component} from 'react';
import Counter from './Counter'
import FileLoader from './FileLoader'

class ZadanieReact extends Component {
    constructor() {
        super();
        this.state = {};
    }


    render() {
        return (
            <div className="container">
                <Counter />
                <FileLoader/>
            </div>
        );
    }

}

export default ZadanieReact;
