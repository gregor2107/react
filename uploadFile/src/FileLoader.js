import React, {Component} from 'react';
import {FormControl, Panel, Row, Col} from 'react-bootstrap';
import FontAwesome from 'react-fontawesome';

class FileLoader extends Component {
    constructor() {
        super();
        this.state = {
            file: '',
            dimension: {
                width: '',
                height: ''
            }
        };
        this.imgLoad = this.imgLoad.bind(this);
    }


    upload(e) {
        let file = e.target.files[0];

        let reader = new FileReader();


        reader.onloadend = () => {

            let isImg = file.type === 'image/jpeg' || file.type === 'image/png';

            this.setState({
                file,
                imagePreviewUrl: isImg ? reader.result : null
            });
        };

        reader.readAsDataURL(file)
    }

    imgLoad(e) {

        let dimension = {
            width: e.target.offsetWidth,
            height: e.target.offsetHeight,
        };
        this.setState({
            dimension
        })
    }


    formatSize(bytes) {
        let sizes = ['Bitów', 'KB', 'MB', 'GB', 'TB'];
        if (bytes === 0) {
            return '0 Bitów'
        }


        let i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        return Math.round(bytes / Math.pow(1024, i)) + ' ' + sizes[i];
    }

    render() {

        const {imagePreviewUrl, file, dimension} = this.state;

        let imgPrev = '';
        let otherFile = '';

        if (imagePreviewUrl) {
            imgPrev = (<div>
                <p>{file.name}</p>
                <p>{this.formatSize(file.size)}</p>
                <p>Szerokość: {dimension.width}</p>
                <p>Wysokość: {dimension.height}</p>
                <img onLoad={this.imgLoad} src={imagePreviewUrl}/>

            </div>);
        } else {
            imgPrev = (<div>Dodaj obrazek aby zobaczyć podgląd</div>);
        }


        if (file && !imagePreviewUrl) {
            otherFile = (<div>
                <p>{file.name}</p>
                <p>{file.type}</p>
                <p>{this.formatSize(file.size)}</p>
            </div>)
        }


        return (
            <Row>
                <Col sm={12} className="text-center">
                    <FormControl  name="upload" id="upload" onChange={this.upload.bind(this)} className="fileUpload" type="file" />
                    <label htmlFor="upload">
                        <span>{file.name}</span>
                        <strong> <FontAwesome name="folder-open" /> File selection</strong>
                    </label>
                    <Panel>{imgPrev}</Panel>
                    <Panel>{otherFile}</Panel>

                </Col>
            </Row>
        );
    }

}

export default FileLoader;